from dotenv import load_dotenv
load_dotenv()

import os
import json
import tempfile
import time

import requests
import cups
from reportlab.pdfgen import canvas
from reportlab_qrcode import QRCodeImage
from reportlab.lib.units import mm


BACKEND_URL =  os.environ.get("BACKEND_URL")
AUTH = (os.environ.get("AUTH_USER"),os.environ.get("AUTH_PASSWORD"))
PRINTER = "dymo" if os.environ.get("PRINTER") == None else os.environ.get("PRINTER")

conn = cups.Connection()


def print_qr(text,name,id):
    with tempfile.NamedTemporaryFile() as tmp:
        c = canvas.Canvas(tmp.name)
        #use 89*36 mm sized sticky 
        c.setPageSize((89*mm,36*mm))
        c.setFont("Helvetica", 6, leading = None)
        c.drawString(39*mm,9*mm,name)
        c.drawString(39*mm,6*mm,id)
        qr = QRCodeImage(text, size=33 * mm)
        qr.drawOn(c, 4*mm, 3*mm)
        c.save()   
        conn.printFile(PRINTER, tmp.name, " ",{}) 

while True:
    call = requests.get(BACKEND_URL,auth=AUTH)
    print(call.status_code, call.reason)
    strings = json.loads(call.text)

    for s in strings:
        call = requests.get(s,auth=AUTH)
        name = json.loads(call.text)["name"]
        id = json.loads(call.text)["id"]
        print_qr(s,name,id)


    requests.put(BACKEND_URL,auth=AUTH,json={"prints":[]})
    time.sleep(5)

