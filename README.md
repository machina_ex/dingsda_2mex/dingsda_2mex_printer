# Dymo Printer on RaspberryPi for dingsda_2mex

This is a print server running on a RaspberryPi minicomputer. 

We use it in our theater/prop storage facility to print QR Code labels directly from the smartphone running the [dingsda2mex client](https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_backend) without having network infrastructure in our store other than our own smartphones.

## Prepare Raspberrypi

1. Use the [Raspi Imager](https://www.raspberrypi.com/software/) to write an SD-card using the newest RaspiOS. Click onto the cog symbol in the lower right corner to setup wifi and enable SSH.
2. Connect to the Raspberrypi using `ssh pi@raspberrypi`
3. Configure its wifi to the credentials of your access point of choice (we use a smartphone in hotspot mode).

## Install CUPS and dymo drivers

Follow the instructions on https://www.baitando.com/it/2017/12/12/install-dymo-labelwriter-on-headless-linux
but use https://github.com/GravisZro/dymo-cups-driver-archive (for Model 550) or https://github.com/matthiasbock/dymo-cups-drivers/blob/master/ppd/lw450.ppd (for Model 450) to download the dymo drivers.

## Install the Dingda2mex script

1. copy this repo on the pi
2. install dependencies `sudo apt install python3-pip python3-cups python3-reportlab && sudo pip3 install reportlab-qrcode && sudo pip3 install python-dotenv` 
3. create a file named `.env` and add the following contents: `BACKEND_URL = https://yourserver.your-domain/api/printer/YOUR_PRINTER/_queue` `AUTH_USER = A VALID USER` `AUTH_PASSWORD = THE USER'S PASSWORD`
  b. test the print server `python3 dingsda2mex_printer.py` if everything works, quit and go on.

  Now you can make it run within whatever daemon/background process management environment you prefer. The following steps are using systemd, but you could of course also install tmux, screen, supervisor or any other program to manage the print service in the background and restart it on system start.

4. open `dingsda2mex_print.service` and adapt `WorkingDirectory=` to the directory the `dingsda2mex_printer.py` file is located in. Do the same with the `ExecStart=` entry.
5. copy the service file into systemds system dir: `sudo ln -s dingsda2mex_print.service /etc/systemd/system`
6. enable service: `sudo systemctl enable dingsda2mex_print`
7. start service using `sudo systemctl start dingsda2mex_print`


8. Start the raspberry pi with the printer connected
9. Connect it to the internet (either via ethernet cable or - as we do it in places where we dont have access to a router - use your mobile phone as a wifi hotspot and [configure the raspberry pi wifi](https://www.raspberrypi.com/documentation/computers/configuration.html) to connect to the internet via our smartphone's wifi) 

## Note
Because the printscript does [polling](https://en.wikipedia.org/wiki/Polling_(computer_science)), it is recommended to switch the raspi off when not in use to reduce unnecessary requests.


## Authors and acknowledgment

gefördert durch Senatsverwaltung für Kultur und Europa Berlin  
<img src="https://machinacommons.world/img/Logo_Senat_Berlin.png" alt="Logo Senatsverwaltung für Kultur und Europa Berlin" width="400"/>
